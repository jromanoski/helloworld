﻿using HelloWorld.Entities;
using Microsoft.Azure.Documents.Client;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace HelloWorld.Controllers.API
{
    public class BlogController : ApiController
    {
        #region Fields
        private SafeHandle _handle = new SafeFileHandle(IntPtr.Zero, true);
        private bool _disposed = false;

        private DocumentClient _client = new DocumentClient(new Uri(ConfigurationManager.AppSettings["DocumentDBUrl"]), ConfigurationManager.AppSettings["DocumentDBKey"]);
        private const string _databaseName = "HelloWorld";
        private const string _collectionName = "Blogs";
        private Uri _documentUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
        #endregion

        #region API Methods
        // GET: api/Blog
        [ResponseType(typeof(IEnumerable<Blog>))]
        public async Task<IHttpActionResult> Get()
        {
            var values = _client.CreateDocumentQuery<Blog>(_documentUri).ToList();
            return Ok<IEnumerable<Blog>>(values);
        }

        // GET: api/Blog/5
        [ResponseType(typeof(Blog))]
        public async Task<IHttpActionResult> Get(string id)
        {
            var value = _client.CreateDocumentQuery<Blog>(_documentUri).Where(b => b.id == id).ToList().FirstOrDefault();
            return Ok<Blog>(value);
        }

        // POST: api/Blog
        [ResponseType(typeof(Blog))]
        public async Task<IHttpActionResult> Post(Blog value)
        {
            var response = await _client.CreateDocumentAsync(_documentUri, value);
            value.id = response.Resource.Id;
            return Ok<Blog>(value);
        }

        // PUT: api/Blog/5
        [ResponseType(typeof(Blog))]
        public async Task<IHttpActionResult> Put(string id, Blog value)
        {
            value.id = value.id != id ? id : value.id;
            await _client.UpsertDocumentAsync(_documentUri, value);
            return Ok<Blog>(value);
        }

        // DELETE: api/Blog/5
        public async Task<IHttpActionResult> Delete(string id)
        {
            await _client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(_databaseName, _collectionName, id));
            return Ok();
        }
        #endregion

        #region Dispose Method
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _handle.Dispose();
                _client.Dispose();
            }

            _disposed = true;
            base.Dispose(disposing);
        }
        #endregion
    }
}
