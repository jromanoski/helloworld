﻿namespace HelloWorld {
    "use strict";

    angular.module("helloworld", [
        "ngRoute",
        "ngSanitize",
        "ui.bootstrap",
        "helloworld.blogs"
    ]);
}
