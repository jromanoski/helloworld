var HelloWorld;
(function (HelloWorld) {
    "use strict";
    angular.module("helloworld", [
        "ngRoute",
        "ngSanitize",
        "ui.bootstrap",
        "helloworld.blogs"
    ]);
})(HelloWorld || (HelloWorld = {}));
//# sourceMappingURL=helloworld.module.js.map