var HelloWorld;
(function (HelloWorld) {
    var Blogs;
    (function (Blogs) {
        "use strict";
        angular.module("helloworld.blogs", []).config(config);
        config.$inject = ["$locationProvider", "$routeProvider"];
        function config($locationProvider, $routeProvider) {
            $locationProvider.html5Mode(false);
            $routeProvider
                .when("/ListBlogs", {
                templateUrl: "/Home/ListBlogs",
                controller: "listblogscontroller as ctrl"
            })
                .when("/AddBlog", {
                templateUrl: "/Home/AddBlog",
                controller: "addblogcontroller as ctrl"
            })
                .otherwise({
                redirectTo: "/ListBlogs"
            });
        }
    })(Blogs = HelloWorld.Blogs || (HelloWorld.Blogs = {}));
})(HelloWorld || (HelloWorld = {}));
//# sourceMappingURL=blogs.module.js.map