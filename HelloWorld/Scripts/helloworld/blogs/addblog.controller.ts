﻿namespace HelloWorld.Blogs {
    "use strict";

    interface IAddBlogScope extends ng.IScope {
        blog: HelloWorld.Blogs.IBlog;
    }

    interface IAddBlogController {
        addBlog(blog: HelloWorld.Blogs.IBlog): void;
    }

    export class AddBlogController implements IAddBlogController {
        static $inject = [
            "$scope",
            "$log",
            "$http",
            "$location",
            "blogsservice"
        ];
        constructor(
            private $scope: IAddBlogScope,
            private $log: ng.ILogService,
            private $http: ng.IHttpService,
            private $location: ng.ILocationService,
            private blogsService: HelloWorld.Blogs.IBlogsService
        ) {
        }

        addBlog(): void {
            this.blogsService.addBlog(this.$scope.blog).then((result: HelloWorld.Blogs.IBlog) => {
                this.$location.path("/#/ListBlogs");
            });
        }
    }

    angular
        .module("helloworld.blogs")
        .controller("addblogcontroller", AddBlogController);
}
