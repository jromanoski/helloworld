﻿namespace HelloWorld.Blogs {
    export interface IBlog {
        id: string;
        Title: string;
        Author: string;
        Content: string;
    }

    export interface IBlogsService {
        getBlogs(): ng.IPromise<IBlog[]>;
        addBlog(blog: IBlog): ng.IPromise<IBlog>;
    }

    export class BlogsService implements IBlogsService {
        constructor(private $http: ng.IHttpService, private $log: ng.ILogService) {
        }

        getBlogs(): ng.IPromise<IBlog[]> {
            return this.$http.get("/api/blog")
                .then((response: ng.IHttpPromiseCallbackArg<IBlog[]>): IBlog[]=> {
                    return response.data;
                });
        }

        addBlog(blog: IBlog): ng.IPromise<IBlog> {
            return this.$http.post("/api/blog", blog)
                .then((response: ng.IHttpPromiseCallbackArg<IBlog>): IBlog =>
                {
                    return response.data;
                });
        }
    }

    factory.$inject = ["$http", "$log"];
    function factory($http: ng.IHttpService, $log: ng.ILogService) {
        return new BlogsService($http, $log);
    }

    angular
        .module("helloworld.blogs")
        .factory('blogsservice', factory);

}
