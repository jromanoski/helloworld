var HelloWorld;
(function (HelloWorld) {
    var Blogs;
    (function (Blogs) {
        var BlogsService = (function () {
            function BlogsService($http, $log) {
                this.$http = $http;
                this.$log = $log;
            }
            BlogsService.prototype.getBlogs = function () {
                return this.$http.get("/api/blog")
                    .then(function (response) {
                    return response.data;
                });
            };
            BlogsService.prototype.addBlog = function (blog) {
                return this.$http.post("/api/blog", blog)
                    .then(function (response) {
                    return response.data;
                });
            };
            return BlogsService;
        }());
        Blogs.BlogsService = BlogsService;
        factory.$inject = ["$http", "$log"];
        function factory($http, $log) {
            return new BlogsService($http, $log);
        }
        angular
            .module("helloworld.blogs")
            .factory('blogsservice', factory);
    })(Blogs = HelloWorld.Blogs || (HelloWorld.Blogs = {}));
})(HelloWorld || (HelloWorld = {}));
//# sourceMappingURL=blogs.service.js.map