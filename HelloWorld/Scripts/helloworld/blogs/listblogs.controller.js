var HelloWorld;
(function (HelloWorld) {
    var Blogs;
    (function (Blogs) {
        "use strict";
        var ListBlogsController = (function () {
            function ListBlogsController($scope, $log, $http, blogsService) {
                this.$scope = $scope;
                this.$log = $log;
                this.$http = $http;
                this.blogsService = blogsService;
                this.getBlogs();
            }
            ListBlogsController.prototype.getBlogs = function () {
                var _this = this;
                this.blogsService.getBlogs().then(function (result) {
                    _this.$scope.blogs = result;
                });
            };
            ListBlogsController.$inject = [
                "$scope",
                "$log",
                "$http",
                "blogsservice"
            ];
            return ListBlogsController;
        }());
        Blogs.ListBlogsController = ListBlogsController;
        angular
            .module("helloworld.blogs")
            .controller("listblogscontroller", ListBlogsController);
    })(Blogs = HelloWorld.Blogs || (HelloWorld.Blogs = {}));
})(HelloWorld || (HelloWorld = {}));
//# sourceMappingURL=listblogs.controller.js.map