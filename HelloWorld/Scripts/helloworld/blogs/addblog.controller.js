var HelloWorld;
(function (HelloWorld) {
    var Blogs;
    (function (Blogs) {
        "use strict";
        var AddBlogController = (function () {
            function AddBlogController($scope, $log, $http, $location, blogsService) {
                this.$scope = $scope;
                this.$log = $log;
                this.$http = $http;
                this.$location = $location;
                this.blogsService = blogsService;
            }
            AddBlogController.prototype.addBlog = function () {
                var _this = this;
                this.blogsService.addBlog(this.$scope.blog).then(function (result) {
                    _this.$location.path("/#/ListBlogs");
                });
            };
            AddBlogController.$inject = [
                "$scope",
                "$log",
                "$http",
                "$location",
                "blogsservice"
            ];
            return AddBlogController;
        }());
        Blogs.AddBlogController = AddBlogController;
        angular
            .module("helloworld.blogs")
            .controller("addblogcontroller", AddBlogController);
    })(Blogs = HelloWorld.Blogs || (HelloWorld.Blogs = {}));
})(HelloWorld || (HelloWorld = {}));
//# sourceMappingURL=addblog.controller.js.map