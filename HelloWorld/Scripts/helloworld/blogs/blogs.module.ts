﻿namespace HelloWorld.Blogs {
    "use strict";

    angular.module("helloworld.blogs", [
    ]).config(config);

    config.$inject = ["$locationProvider", "$routeProvider"];
    function config($locationProvider: ng.ILocationProvider, $routeProvider: ng.route.IRouteProvider): void {
        $locationProvider.html5Mode(false);
        $routeProvider
            .when("/ListBlogs", {
                templateUrl: "/Home/ListBlogs",
                controller: "listblogscontroller as ctrl"
            })
            .when("/AddBlog", {
                templateUrl: "/Home/AddBlog",
                controller: "addblogcontroller as ctrl"
            })
            .otherwise({
                redirectTo: "/ListBlogs"
            });
    }
}
