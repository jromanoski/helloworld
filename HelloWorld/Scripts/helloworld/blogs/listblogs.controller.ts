﻿namespace HelloWorld.Blogs {
    "use strict";

    interface IListBlogsScope extends ng.IScope {
        blogs: HelloWorld.Blogs.IBlog[];
    }

    interface IListBlogsController {
        getBlogs(): void;
    }

    export class ListBlogsController implements IListBlogsController {
        static $inject = [
            "$scope",
            "$log",
            "$http",
            "blogsservice"
        ];
        constructor(
            private $scope: IListBlogsScope,
            private $log: ng.ILogService,
            private $http: ng.IHttpService,
            private blogsService: HelloWorld.Blogs.IBlogsService
        ) {
            this.getBlogs();
        }

        getBlogs(): void {
            this.blogsService.getBlogs().then((result: HelloWorld.Blogs.IBlog[]) => {
                this.$scope.blogs = result;
            });
        }
    }

    angular
        .module("helloworld.blogs")
        .controller("listblogscontroller", ListBlogsController);
}
